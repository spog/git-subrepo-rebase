Source: git-subrepo-rebase
Section: vcs
Priority: optional
Homepage: https://github.com/ingydotnet/git-subrepo
Vcs-Browser: https://salsa.debian.org/dxld-guest/git-subrepo
Vcs-Git: https://salsa.debian.org/dxld-guest/git-subrepo.git
Maintainer: Daniel Gröber <dxld@darkboxed.org>
Build-Depends:
 debhelper-compat (= 12),
 debhelper (>= 12),
 git,
 bash-completion
Rules-Requires-Root: no
Standards-Version: 4.5.0

Package: git-subrepo
Architecture: any
Depends: ${misc:Depends}, git
Description: Alternative to git-submodule(1) and git-subtree(1)
 git-subrepo allows embedding and managing copies of other git repositories
 in your repo for purposes such as vendoring.
 .
 Commits in the parent repo involving the subrepo can easily be pushed back
 upstream even when they touch files outside the subrepo -- these will simply
 be omitted.
 .
 Pulling new upstream commits back into your repo is naturally also possible.
 .
 git-subrepo is designed such that only the maintainer of a repo will
 actually need to have it installed, contributors only need to do so if
 they wish to push/pull from the subrepo's upstream and user should never
 have to interact with git-subrepo at all since all of a subrepos file are
 available right after a plain git-clone.
 .
 This is unlike git-submodule(1)s where all users and contributors must be
 aware of their presence and deal with them.
 .
 git-subrepo is in principle somewhat similar to git-subtree(1) as it also
 embedds snapshot copies of other repos, however it is much easier to use
 and the way the history is kept is less convoluted. Pulling any number of
 new commits from a subrepo's upstream will result in only a single commit
 in the parent repo for example.
